<?php

    require('animal.php');
    require('frog.php');
    require('ape.php');

$shaun = new Animal('Shaun');

echo "Nama Binatang : " . $shaun->name . "<br>";
echo "Jumlah Kaki : " . $shaun->legs . "<br>";
echo "Berdarah Dingin : " . $shaun->cold_blooded . "<br> <br>";

$frog = new Frog('Buduk');

echo "Nama Binatang : " . $frog->name . "<br>";
echo "Jumlah Kaki : " . $frog->legs . "<br>";
echo "Berdarah Dingin : " . $frog->cold_blooded . "<br>";
$frog->jump();
echo "<br>";

$ape = new Ape('Kera Sakti');

echo "<br>";
echo "Nama Binatang : " . $ape->name . "<br>";
echo "Jumlah Kaki : " . $ape->legs . "<br>";
echo "Berdarah Dingin : " . $ape->cold_blooded . "<br>";
$ape->yell();